%% Millikan

clear all;
%   round = @(a,b,c) a; %%Comment to allow rounding
%% Konstanten
%%
g = 9.81
rho_Oel = 875.3
rho_Luft = 1.29
eta = 1.81e-5
UC = 500
sSk1 = 0.1e-3
V = 1.875
d = 6e-3
b = 6e-8 %Cunningnahm Korrekturfaktor
%% Messdaten
%%
t{1,1} = [25.13 25.14 24.82 24.70 25.09 25.16 25.12 25.15 24.76 25.12];
t{1,2} = [10.70 10.51 10.41 10.33 10.45 10.62 10.20 10.49 10.66 10.22];
t{2,1} = [11.68 11.72 11.66 11.34 11.47 11.55 12.13 11.75 11.96 11.58];
t{2,2} = [10.88 10.89 10.69 11.22 11.04 11.20 11.30 10.77 11.13 11.00];
t{3,1} = [ 9.91  9.92 10.07 10.00 10.06 10.15  9.87  9.86  9.68 10.02];
t{3,2} = [10.04 10.12  9.89  9.73 10.17  9.69 10.25 10.17  9.97 10.06];
t{4,1} = [27.19 27.18 26.92 26.81 27.00 26.77 26.88 26.80 26.92 27.13];
t{4,2} = [31.23 30.96 31.20 31.21 31.30 31.30 30.96 30.93 31.11 30.97];
t{5,1} = [17.90 17.90 17.59 17.91 17.69 18.19 18.18 18.03 17.92 18.03];
t{5,2} = [16.95 17.03 16.62 16.84 16.60 16.88 16.87 17.06 17.02 16.93];
t
nT = size(t,1)
sSk = 20
%% Fehlerannahmen
%%
dt = 0.2;
skt = 20
dSk = 0.5
%% Mittelwerte
%%
tMean = cellfun(@mean,t);
tMean = round(tMean,3,'decimal')
tStd = cellfun(@std,t);
tStd = round(tStd,2,'significant')
tMeanStd = cellfun(@(c) std(c)./sqrt(numel(c)),t);
tMeanStd = round(tMeanStd,2,'significant')
%% Geschwindigkeiten
%%
s = sSk * sSk1 / V;
s = round(s,4,'significant')
smm = s*1000
ds = sSk1 / V * dSk;
ds = round(ds,2,'significant')


v = s ./ tMean;
v = round(v,7,'decimal')

dv = sqrt((s./tMean.^2 .*tMeanStd).^2 + (1./tMean.*ds).^2);
dv = round(dv,2,'significant')
dv_rel = sqrt((ds / s)^2 + (tMeanStd./tMean).^2);
dv_rel = round(dv_rel,2,'significant')
dv_test = dv_rel .* v;
dv_test = round(dv_test,2,'significant');

%% Tr�pfchenradius
%%
a = sqrt(9*eta / (2*g*(rho_Oel - rho_Luft)));
a = round(a,4,'significant')
r = a*sqrt(v(:,1));
r = round(r,9,'decimal')
dr = 0.5*a./sqrt(v(:,1)) .* dv(:,1);
dr = round(dr,9,'decimal')
%% Ladungen
%%
q = 6*pi*eta *  d * r/UC .* (v(:,1) + v(:,2)) ;
q = round(q,21,'decimal')

tmp = 6*pi*eta*d / UC;
dq = sqrt( (tmp * (v(:,1) + v(:,2)) .*dr).^2 + (tmp * r .* dv(:,1)).^2 + (tmp * r .* dv(:,2)).^2);
dq = round(dq,21,'decimal')

qk = q ./ sqrt((1 + b ./ r).^3);
qk = round(qk,21,'decimals')

dqk = sqrt( (3*b*q ./ (2*r.^2) .* (1 + b./r).^(-5/2) .* dr).^2 + ((1 + b./r).^(-3/2) .* dq).^2);
dqk = round(dqk,21,'decimal')

%% Plot
%%
errorbar((1:nT)',qk,dqk,'vertical','o')
%% Elementarladung
%%
eTmp = min(qk);
en = round(qk / eTmp)
den = dqk / eTmp;
errorbar((1:nT)',en,den,'vertical','o'), grid on
e = mean(qk ./ en);
de = sqrt(sum((dqk ./ (en * nT)).^2));
e = round(e,4,'significant')
de = round(de,2,'significant')

%%