%% Wie rechne ich... Fehler des Mittelwerts und Standardabweichung?

clear all;

Z = 3.4e3
dZ = 0.1e3
dZrel = dZ / Z

omega = 5e5
dOmega = 1
dOmegaRel = dOmega / omega

L = 10e-3
dLrel = 0.03
dL = L * dLrel

C = 250e-12
dCrel = 0.02
dC = C*dCrel

%% R
%%
R = sqrt(Z^2 - (omega*L - 1/(omega*C))^2)
%% Fehlerrechnung Schrittweise
%%
dRSqdZ = 2*Z
dRSqdL = omega * 2*(omega*L - 1/(omega*C))
dRSqdC = 1/(omega*C^2) * 2*(omega*L - 1/(omega*C))
dRSq = sqrt((dRSqdZ * dZ)^2 + (dRSqdC*dC)^2 + (dRSqdL*dL)^2)
dR = dRSq / sqrt(R^2)
%%