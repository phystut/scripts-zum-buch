%% Wie rechne ich... Fehler des Mittelwerts und Standardabweichung?

clear all;

%Gegeben
data = [12.5 17.0 8.4 8.7 15.5 15.9 16.6 14.9 12.4 15.2]
n = numel(data)
%Oder zuf�llig generieren
%n = 10
%data = 13.71 + 3.11*randn(1,n);
%% Statistik
%%
dataMean = mean(data)
dataStd = std(data)

dataMeanStd = dataStd / sqrt(n)

histogram(data,'Normalization','probability'); hold on;

%% Vergleich mit Gau�verteilung
%%
dataMinMax = get(gca,'XLim');%minmax(data);

gaussX = linspace(dataMinMax(1),dataMinMax(2),100);
gaussY = normpdf(gaussX,dataMean,dataStd);
gaussYmean = normpdf(gaussX,dataMean,dataMeanStd);

plot(gaussX,gaussY)
plot(gaussX,gaussYmean)


%%