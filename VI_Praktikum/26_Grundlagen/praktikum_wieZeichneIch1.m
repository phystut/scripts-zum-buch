%% Wie zeichne ich... eine Exponentialfunktion auf einfach-logarithmischem Papier?

clear all;

U = [28.6 13.4 7.67 4.11 1.94]
dUrel = 0.05;
dU = dUrel*U
x = [5 10 15 20 25]
dx = 0.2*ones(1,numel(x))

%% Plot
%%
errorbar(x,U,dU,dU,dx,dx,'o'), grid on

errorbar(x,U,dU,dU,dx,dx,'o'), grid on
set(gca,'YScale','log')

%% Gerade
%%
U1 = 34.4e-3
U2 = 1.7e-3

lambda = -((log(U2) - log(U1)) / (27e-3 - 3e-3))^(-1)

lambdaF = -((log(1.2e-3) - log(47.2e-3)) / (27e-3 - 3e-3))^(-1)
lambdaF = (27e-3 - 3e-3) / (log(47.2e-3) - log(1.2e-3))

dLambda = abs(lambda - lambdaF)
%%