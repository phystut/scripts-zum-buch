# Physiktutorium - Scripts zum Buch

Hier findet ihr alle Matlab-Skripte zum Buch "Tutorium Physik fürs Nebenfach"!

Die Skripte richten sich nach der zweiten Auflage und sind nach Teil und Kapitel gegliedert und dabei nach Aufgabennummer bzw. Kasten benannt.