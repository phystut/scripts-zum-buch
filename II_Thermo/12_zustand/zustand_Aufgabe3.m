%% Aufgabe 3

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
kappa = 4/3
relV = 1e-9
T = 3000

%% Temperatur
%%
T2 = T * relV^(kappa - 1)
%%