%% Wie rechne ich... eine isotherme Zustandsänderung?

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T1 = 30 + TZeroK
T2 = 4 + TZeroK
p1 = pStd
A = 0.5


%% $$\frac{p}{T} = const.$$
%%
p2 = T2*p1/T1
deltaP = p1 - p2

%% Kraft an Tür
%%
F = deltaP*A
%% G
%%
m = F / g