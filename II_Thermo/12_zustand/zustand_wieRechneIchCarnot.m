%% Wie rechne ich... einen Carnot-Prozess?

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
n = 0.1
V1 = 2e-3 %2l
V2 = 0.1e-3
TW = 100 + TZeroK
TK = 0 + TZeroK

%% Arbeit pro Zyklus
%%
deltaT = TW - TK
W = - n*R*log(V2/V1)*deltaT

%% Wirkungsgrad
%%
wGrad = deltaT / TW

%% Wärmeenergie
%%
Q = abs(W) / wGrad

%%