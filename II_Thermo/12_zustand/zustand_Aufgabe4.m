%% Aufgabe 4

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T1 = 250
T2 = 330

relV = 4
n = 2

%% a) Wirkungsgrad
%%
deltaT = T2 - T1
eta = deltaT / T2
%% b) Arbeit
%%
W = -n*R*log(relV)*deltaT
%% c) Energie
%%
Q = abs(W) / eta
%%