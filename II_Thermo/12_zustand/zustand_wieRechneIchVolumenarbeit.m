%% Wie rechne ich... die Volumenarbeit eines idealen Gases aus?

clear all;

k_B = 1.38064852e10-23
R = 8.3144598

T = 293
V1 = 1 %l
V2 = 2 %l


n = 1 %mol

%% Anzahl Möglichkeiten
%%
W = -n*R*T * log(V2 / V1)