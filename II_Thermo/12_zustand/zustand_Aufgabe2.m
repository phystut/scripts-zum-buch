%% Aufgabe 2

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
pAussen = 1010e2
T = 5 + TZeroK
deltaT = 70
A = 0.03

%% $$\frac{p}{T} = const.$$
%%
p2 = pAussen * (T+deltaT) / T

%% Kraft
%%
F = (p2 - pAussen) * A
%%