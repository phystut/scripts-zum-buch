%% Wie rechne ich... eine isotherme Zustandsänderung?

clear all;

k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TRaum = 293

V1 = 1 %l
h = 2e3
p2 = 850e2



%% $$p \cdot V = const.$$
%%
V2 = pStd / p2 * V1