%% Aufgabe 1

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T1 = 22 + TZeroK
V = 1.5e-3
T2 = -9 + TZeroK

%% $$\frac{V}{T} = const.$$
%%
V2 = T2 * V / T1
V2 / V
%%