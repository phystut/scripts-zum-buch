%% Aufgabe 4

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
m = 12
etaGes = 0.1
etaStir = 0.6
deltaT = -15
cH20 = 4181
%% b) Energiešnderung
%%
deltaQ = cH20*m*deltaT
W = abs(deltaQ / etaGes)
WkCal = W/cH20