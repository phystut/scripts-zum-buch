%% Wie rechne ich... eine isotherme Zustandsänderung?

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T1 = 20 + TZeroK
p1 = 2.5*1e5
p2 = 5*1e5
kappa = 1.4


%% $$p^{1-\kappa}T^\kappa = const.$$
%%
T2 = (p1^(1-kappa) * T1^kappa / p2^(1-kappa))^(1/kappa)

%%