%% Wie rechne ich... die Durchschnittsgeschwindigkeit in einem Gas aus?

clear all;

%Konstanten
k_B = 1.38064852e-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
N = 3
f(1) = 3
f(2) = 3
f(3) = 3*N - 6
%% Adiabatenexponent
%%
kappa = (sum(f)+2)/sum(f)