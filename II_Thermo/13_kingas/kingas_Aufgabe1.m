%% Wie rechne ich... die Durchschnittsgeschwindigkeit in einem Gas aus?

clear all;

%Konstanten
k_B = 1.38064852e-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben

aufgabe = 'abcd';
M = [4 131 44 44]*1e-3
T = [300 300 350 600]
%% Wahrscheinlichste und mittlere Geschwindigkeit
%%
vMaxProb = sqrt(2*R*T./M);
vMean = 2/sqrt(pi)*vMaxProb;

%Ausgabe
for i = 1:numel(aufgabe)
    fprintf('Teilaufgabe %s)\n\tvMaxProb = %f\n\tvMean = %f\n',aufgabe(i),vMaxProb(i),vMean(i));
end