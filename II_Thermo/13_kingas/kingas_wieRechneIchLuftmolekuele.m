%% Wie rechne ich... die Geschwindigkeit der Luftmoleku�le im H�rsaal aus? (Vol. 1)

clear all;

%Konstanten
k_B = 1.38064852e10-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
rho = 1.250
%% Mittlere Geschwindigkeit
%%
vMean = sqrt(3*pStd / rho)