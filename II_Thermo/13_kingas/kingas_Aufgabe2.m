%% Wie rechne ich... die Durchschnittsgeschwindigkeit in einem Gas aus?

clear all;

%Konstanten
k_B = 1.38064852e-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
aufgabe = 'abcd';

%% Freiheitsgrade
%%
f(1) = 3;
f(2) = 6 + 1;
f(3) = 2;
f(4) = f(2) - 1

%% Adiabaten
%%
kappa = (f + 2) ./ f;

%Ausgabe
for i = 1:numel(aufgabe)
    fprintf('Teilaufgabe %s)\n\tkappa= = %f\n',aufgabe(i),kappa(i));
end