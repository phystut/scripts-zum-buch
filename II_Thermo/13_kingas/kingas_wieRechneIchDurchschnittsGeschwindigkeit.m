%% Wie rechne ich... die Durchschnittsgeschwindigkeit in einem Gas aus?

clear all;

%Konstanten
k_B = 1.38064852e-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T = 20 + TZeroK
MCO2 = 44e-3
%% Wahrscheinlichste und mittlere Geschwindigkeit
%%
vMaxProb = sqrt(2*R*T/MCO2)

vMean = 2/sqrt(pi)*vMaxProb