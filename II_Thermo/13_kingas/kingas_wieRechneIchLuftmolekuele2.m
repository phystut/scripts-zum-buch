%% Wie rechne ich... die Geschwindigkeit der Luftmoleku�le im H�rsaal aus? (Vol. 1)

clear all;

%Konstanten
k_B = 1.38064852e-23
R = 8.3144598
pStd = 1013.25e2
TZeroK = 273.15
g = 9.81

%Gegeben
T = 0 + TZeroK
m = 4.6e-26
%% Mittlere Geschwindigkeit
%%
vMean = sqrt(3*k_B*T / m)