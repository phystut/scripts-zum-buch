%% Wie rechne ich... mit der mikroskopischen Definition von Entropie?

clear all;

n = 4
k = 3

k_B = 1.38e-23

%% Anzahl Möglichkeiten
%%
Omega = nchoosek(n,k) %Binomialkoeffizient
%% Entropie 
%%
S = k_B * log(Omega)
%% Mehr Möglichkeiten
%%
n2 = 8
Omega2 = nchoosek(n2,k)
S = k_B * log(Omega2)