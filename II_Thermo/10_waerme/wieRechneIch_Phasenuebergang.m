%% Wie rechne ich... die Endtemperatur f�r ein thermisches Gleichgewicht aus?

mEis = 0.250
TEis = 0

TTee = 90

hSEis = 344e3 %J/g
cH2O = 4.18e3
%% Schmelzenergie
%%
deltaHS = mEis*hSEis
%% Erw�rmung
%%
deltaQ = mEis * cH2O * (TTee - TEis)

%% Gesamtenergie
%%
deltaQges = deltaHS + deltaQ