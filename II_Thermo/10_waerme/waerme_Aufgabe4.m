%% Aufgabe 4

clear all;

m_Pb = 10
deltaT = 5
c_Pb = 4000
DeltaT = 5
T_Pb = 60
T_Lu = 15
m_Lu = 240
V = 10 * 10 * 2
c_Lu = 1012

%% $$\Delta Q$$
%%
deltaQ = c_Pb * m_Pb * DeltaT

%% Gleichgewichtstemperatur
%%
T_Gg = (c_Pb * m_Pb * T_Pb + c_Lu + c_Lu * m_Lu * T_Lu) / (c_Pb * m_Pb + c_Lu * m_Lu)
%% 
%