%% Wie rechne ich... die W�rmeleitung zwischen zwei Fl�chen aus?

TH = 400 %�C
TP = 25 %�C

r = 0.15
d = 2e-3

lambda_Al = 236

alpha_Al = 23e-6

%% Fl�che
%%
A = pi*r^2
%% W�rmeleitung
%%
QPunkt = lambda_Al * A / d * (TH - TP)
%% 
%