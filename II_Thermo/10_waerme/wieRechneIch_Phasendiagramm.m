%% Wie rechne ich... dmit der Information aus einem Phasendiagramm?

mEis = 0.250
TEis = 0

TTee = 90

Tsied = 70

hSEis = 334e3
hVWasser = 2260e3
cH2Ol = 4.18e3
cH2Og = 2.08e3
%% Schmelzenergie Eis
%%
deltaHS = mEis*hSEis
%% Erw�rmung Fl�ssig
%%
deltaQl = mEis * cH2Ol * (Tsied - TEis)
%% Verdampfen
%%
deltaHV = mEis * hVWasser
%% Erw�rmung Dampf
%%
deltaQd = mEis * cH2Og * (TTee - Tsied)
%% Gesamtenergie

deltaHv = deltaHS + deltaQl + deltaHV + deltaQd