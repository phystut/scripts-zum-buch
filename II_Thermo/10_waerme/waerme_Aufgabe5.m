%% Aufgabe 5

clear all;

Mx = 23 %g/mol
m = 69 %g
N_2 = 4.173e24

N_A = 6.022e23
%% Stoffmenge in Mol
%%
n = m / Mx
%% Teilchenzahl
%%
N = n*N_A

%% Stoffmenge in Mol aus Teilchenzahl
%%
n_2 = N_2 / N_A
%% Deren Masse
%%
m_2 = Mx * n_2
%% 
%