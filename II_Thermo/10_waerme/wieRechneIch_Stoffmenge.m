%% Wie rechne ich... mit Stoffmenge anstatt Masse?

mFeU = 55.8 %u
mFe = 1 %kg

nH2O = 8 %mol
mHU = 1.01 %u
mOU = 16 %u
%% Masse Eisen zu Stoffmenge 
%%

n = mFe / mFeU * 1000
%% Mol H20 zu Masse 
%%
MH2O = 2*mHU + mOU
mH2O = nH2O * MH2O
%% 
%