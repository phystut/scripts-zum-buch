%% Wie rechne ich... die Temperaturänderung bei Änderung der Wärmeenergie?

P = 2.5e3
V = 0.75 %l
T = 20 %°C
t = 1.5*60

cH2O = 4.181e3
%% Masse
%%
m = V % da liter <-> kg
%% Wärmeenergie
%%
deltaQ = P*t
%% Temperaturänderung
%%
deltaT = deltaQ / (cH2O * m)
T + deltaT
%% 
%