%% Aufgabe 3

a = 0.27
b = 0.21
c = 0.05
rho = 2.5e-3 / 1e-6

g = 9.81;
%% a) Trägheitsmoment
%%
m = rho * (a*b*c)
J = 0.5*m*(a^2 + b^2)
%% b) Drehmoment
%%
Erot = 120
omega = sqrt(2*Erot / J)
f = omega / (2*pi)
%% c) Änderung 
%%
l = 0.105
%% 
%