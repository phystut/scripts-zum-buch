%% Wie rechne ich... die Verformung eines Blocks unter einer Kraft?

dl = 0.1e-3
l = 1e-2 % Dicke
a = 0.1 %Seitenl�nge a
b = a %Seitenl�nge b

E_Pb = 20e9
E_Au = 80e9

g = 9.81;
%% Kraft
%%
A = a*b

F = A * E_Pb * dl/l

%% Masse-�quivalent
%%
m = F / g

%% Gold
%%
F = A * E_Au * dl/l
%% 
%