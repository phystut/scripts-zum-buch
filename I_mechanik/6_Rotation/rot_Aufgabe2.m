%% Aufgabe 2

rho_Eiche = 671
alpha = 5
h = 35
r = 1.41
F = 330e3

g = 9.81;
%% Mastgewicht
%%
m = r^2*pi*h*rho_Eiche
m_t = m / 1e3

%% Drehmoment
%%
F_N = sind(alpha) * m * g
M = F_N * h/2

%% H�he zum Anbringen
%%
hSeil = M / F + h/2
%% 
%