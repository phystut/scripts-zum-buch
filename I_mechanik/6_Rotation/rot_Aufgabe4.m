%% Aufgabe 4

r = 0.033
hZ = 0.168
rho = 1.05e-3 / 1e-6

alpha = 10

h = 1.2

g = 9.81;
%% a) Trägheitsmoment
%%
m = rho * r^2 * pi * hZ
J = 0.5*m*r^2
%% b) Geschwindigkeit
%%
v = sqrt(m*g*h / (0.5*m + 0.5*J/r^2))
%% 
%