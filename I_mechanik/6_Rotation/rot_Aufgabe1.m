%% Aufgabe 1

v = 29780
r = 149e9

g = 9.81;
%% a) (Kreis)Frequenz
%%
U = 2*r*pi
T = U / v
T_d = T / 3600 / 24

f = 1 / T
w = 2*pi*f

a = v^2/r
%% b) Schwerpunkt
%%
rBeide = [0 r]
mBeide = [1.99e30 5.97e24]

rS = 1/sum(mBeide) * sum(mBeide.*rBeide)

%% c) Drehimpuls
%%
L = r*mBeide(2)*v

rNew = 100e9
vNew = L / (rNew*mBeide(2))
%% 
%