%% Wie rechne ich... Einen Schwerpunkt von drei Massepunkten aus?

m = [1 3 1] 


x = [-4 1 1; -4 -1 3]

g = 9.81;
%% Schwerpunkt
%%
r = 1/sum(m) * x * m'

plot(x(1,:),x(2,:),'LineStyle','None','Marker','*','MarkerSize',12);
hold on;
grid on
plot(r(1),r(2),'LineStyle','None','Marker','x','MarkerSize',15)
%% 
%