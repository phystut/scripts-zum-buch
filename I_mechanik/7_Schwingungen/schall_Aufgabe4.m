%% Aufgabe 4
%%
f_Z = 450;
f_E = 509.4;
L_DI = 40; 
c_S = 343;
r_S = 2;
r_D = 10000; %10km
I0 = 1e-12;

%% a)
%%
v_Z = -c_S * (f_Z/f_E - 1)
v_Z_kmh = v_Z*3.5
%% c)
%%
I_D = 10^(L_DI/10) * I0
I_S = I_D * r_D^2/(r_S^2)
L_S = 10 * log10(I_S / I0)
%% d)
%%
v_S = c_S * (2*(c_S - v_Z)/(c_S+v_Z) - 1)/(2*(c_S-v_Z)/(c_S+v_Z) + 1)
v_S_kmh = v_S * 3.6