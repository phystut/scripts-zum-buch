%% Wie rechne ich... (in)elastischesn Sto�?

m1 = 2e3
m2 = 5e3

v1 = 6
v2 = 0


vElas = @(v1,v2,m1,m2) (m1 - m2)/(m1 + m2) * v1 + 2*m2*v2 / (m1 + m2);
%% Elastischer Sto�
%%
v1s = vElas(v1,v2,m1,m2)
v2s = vElas(v2,v1,m2,m1)
%% Inelastischer Sto�
%%
vs = (m1*v1 + m2*v2) / (m1 + m2)
%% Verformungsenergie
%%
Ekin_1 = 0.5*m1*v1^2
EkinSt = 0.5*(m1 + m2)*vs^2

%% 
%