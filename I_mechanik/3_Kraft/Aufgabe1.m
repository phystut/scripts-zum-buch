%% Aufgabe 1

%Erde
R = 6371e3
M = 5.97e24
G = 6.67e-11
%% a) Umlaufzeit ISS
%%
h = 400e3
rISS = R + h;
%FG = FZ
v = sqrt(G*M / rISS)
U = 2*pi*rISS
tISS = U / v
tISSmin = tISS/60
%% b) Geostätionärer Satellit
%%
t = 24*3600
rGeo = nthroot(G*M / (2*pi / t)^2,3 )
hGeo = rGeo - R
%%