%% Aufgabe 2

%Erde
F = 750
alpha = 30
m = 90

%% Kraftkomponenten
%%
Fx = cosd(alpha)*F
Fy = sind(alpha)*F
%% Beschleunigung
%%
a = Fy/m
%%