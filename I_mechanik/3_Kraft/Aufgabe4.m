%% Aufgabe 4

h = 20
alpha = 40
v0 = 100
g = 9.81
%% Wie nah?
%%
v0x = cosd(alpha)*v0
v0y = sind(alpha)*v0

s = v0x*(v0y + sqrt(v0y^2 + 2*g*h))/g

t = s / v0x