%% Stokes

clear all
r = 2e-3
rhoW = 1400
rhoS = 2700
eta = 10
g = 9.81

%% Geschwindigkeit
%%
v = 9 * g * r^2 * (rhoS - rhoW) / (2*eta)