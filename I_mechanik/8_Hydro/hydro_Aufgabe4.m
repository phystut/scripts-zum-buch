%% Aufgabe 4

h = 1; %m
d = 0.6; %m
dZ = 0.004;
lZ = 0.15;

g = 9.81

V = 500; %ml
N = 10;

eta = 0.001; %Pa s
rho = 1e3; %kg/m^3
%% Abgeleitete Gr��en
%%
r = dZ/2
A = pi*r^2
%% a)

pB = rho*h*g %Pa
Vdot = (pi*r^4)/(8*eta*lZ) * pB %m^3 / s
VdotMl = Vdot * 1e6
deltaT = N*V/VdotMl %s
%% b)
%%
v = Vdot / A
%% c)
%%
nr = nthroot(2,4)
r2 = nr*r