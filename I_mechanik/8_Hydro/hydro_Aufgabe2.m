%% Aufgabe 2

p_L = 1013; %hPa
p_1 = 1013;
p_2 = 1020;
p_3 = 990;

rho_Hg = 13.5;
alpha_W = 20; %Grad
gamma_W = 72.8; %mN/m

alpha_Hg = 140;
gamma_Hg = 470;

d = 4; %mm

g = 9.81;

%% b)
%%
mmSaeule = @(dP,rho) dP*100/(g*rho)*1000; % rho in kg/m^3
p_mmW_2 = mmSaeule(p_2-p_L,rho_W*1000)
p_mmW_3 = mmSaeule(p_3-p_L,rho_W*1000)
p_mmHg_2 = mmSaeule(p_2-p_L,rho_Hg*1000)
p_mmHg_3 = mmSaeule(p_3-p_L,rho_Hg*1000)
%% d)
%%
hk = @(gamma,alpha,rho,r) (2*gamma*cosd(alpha))/(rho*g*r);
hk_W = hk(gamma_W,alpha_W,rho_W*1000,d/2*0.001)
hk_Hg = hk(gamma_Hg,alpha_Hg,rho_Hg*1000,d/2*0.001)