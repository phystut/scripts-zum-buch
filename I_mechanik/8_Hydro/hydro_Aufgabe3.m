%% Aufgabe 3

Vdot = 5.5; %l/min
r = 1.1; %cm

nK = 10e9;
dK = 7e-6; %m

%% a)
%%
vA = (Vdot*0.001/60)/(pi*(r*0.01)^2) * 100
%% b)
%%
A = nK*pi*(dK/2)^2
vK = (pi*(r*0.01)^2 *vA/100)/A