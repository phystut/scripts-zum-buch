%% Aufgabe 4

h = 10
m = 200
R_L = 12


g = 9.81 %m/s^2
%% a)
%%
h
%% b)
%%
ETop = m*g*2*R_L;
Epot = m*g*h;
EkinMin = ETop - Epot;

v = sqrt(2*EkinMin / m)
v_kmh = v * 3.6
%% c)
%%
vTopMin = sqrt(g*R_L) %FZ = FG
Etop = m*g*2*R_L + 0.5*m*vTopMin^2;
Ekin0 = Etop - m*g*h;
vAnfang = sqrt(2*Ekin0 / m)
%% d)
%%
F = 800;

vEnd = sqrt(2*Etop / m)
a_B = F / m;

t = vEnd / a_B