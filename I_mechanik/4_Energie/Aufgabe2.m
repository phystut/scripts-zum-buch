%% Aufgabe 2

v1 = 150 / 3.6 %150 km/h -> m/s
m1 = 900 %kg
v2 = 100 / 3.6
m2 = 6e3

g = 9.81 %m/s^2
%% Kinetische Energien
%%
E1 = 0.5*m1*v1^2
E2 = 0.5*m2*v2^2

E2 > E1