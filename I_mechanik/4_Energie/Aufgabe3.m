%% Aufgabe 3

v = 100 / 3.6 %100 km/h -> m/s
m = 900 %kg
h1 = 75

g = 9.81 %m/s^2
%% a)
%%
Epot = m*g*h1
Ekin = 0.5*m*v^2

Etot = Epot + Ekin

vmax = sqrt(2*Etot / m)
vmax_kmh = vmax*3.6
%% b)
%%
h2 = 97
Epot2 = m*g*h2
Epot2 < Etot