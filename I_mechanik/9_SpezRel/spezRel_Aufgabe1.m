%% Aufgabe 1
%%
l = 12
A = l^2
R_E = 6371e3
n = 8
t = 1
c = 299792458
%% a)
%%
U_E = 2*pi*R_E
v_T = n * U_E / 1
v_T / c
%% b)
%%
n2 = 7
v_T2  = n2 * U_E / 1
v_T2 / c
%% c)
%%
gamma = 1/sqrt(1 - v_T2^2 / c^2)
lK = l / gamma
%% d)
%%
m = 120
mrel = m * gamma
%%