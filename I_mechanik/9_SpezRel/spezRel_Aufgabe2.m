%% Aufgabe 1
%%
h = 15e3
c = 299792458
v = 0.999*c
t = 2.2e-6
%% a) Ohne Relativitätstheorie
%%
s = v*t
s < h
%% b) Zeitdilatation
%%
gamma = 1/sqrt(1 - v^2 / c^2)
tDila = t*gamma
sNew = v*tDila

%% c System des Myons
%%
hKontrakt = h / gamma
%% 
%%