%% Aufgabe 8

clear all;
%% Gegeben:
%%
l = 0.02
d = 0.05
N = 1000
dIdT = 5e-3
I = 1
%% Induktivitšt
%%
mu0 = 1.2566e-6;
L = mu0 * pi * (d/2)^2 * N^2 / l
%% Induktionsspannung
%%
Uind = -L * dIdT
%% Magnetische Feldenergie
%%
W = 0.5 * L * I^2
%% 
%% 
%%