%% Aufgabe 10

clear all;
%% Gegeben:
%%
U0 = 10e3
f = 40
L = 0.5
%% a) Blindwiderstand und Stromstärke
%%
w = 2*pi*f
XL = w*L

Ueff = U0/sqrt(2)
Ieff = Ueff / XL
%% Kondensator
%%
C = 10e-6
%% b) Blindwiderstand und Stromstärke
%%
XC = 1/(w*C)
Ieff = Ueff / XC
%% Widerstand
%%
R = 150
%% c) Impedanz und effektive Stromstärke und Phasenwinkel
%%
Z = sqrt(R^2 + (XL - XC)^2)
Ieff = Ueff / Z
phi = atand((XL - XC)/R)
%% 
%% 
%%