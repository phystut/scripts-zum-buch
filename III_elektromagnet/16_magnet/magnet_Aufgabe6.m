%% Aufgabe 6

clear all;
%% Gegeben:
%%
x = 0.03
Rinnen = 0.03
dBdt = 0.05

%% a) Strom
%%
Uind = - x^2 * dBdt
I = Uind/Rinnen
%% Drehende Schliefe
%%
alpha = 45
dt = 1
B = 0.3
%% b) Induktionsspannung
%%
dAdt = x^2 * sind(alpha) / dt
Uind = - dAdt*B
%% 
%%