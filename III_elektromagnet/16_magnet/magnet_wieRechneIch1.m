%% Wie rechne ich.. eine Induktionsspannung aus?

clear all;
%% Ladungen & Punkt
%%
v = 0.05 %m/s
s1 = 0.1 %m
s2 = s1
x = 0.05 %m
B = 2 %T
N = 1%
%% Bis zum Feld
%%
t1 = s1 / v
Uind1 = 0
%% Eintritt ins Feld
%%
t2 = x / v
Uind2 = -B * x * v
%% Komplett im Feld
%%
s3 = 0.05
t3 = s3 / v
%% Austritt
%%
t4 = t1
Uind = B * x * v
%% Fluss
%%
Phi = N*B*x^2