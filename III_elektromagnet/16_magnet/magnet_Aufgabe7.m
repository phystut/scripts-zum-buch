%% Aufgabe 7

clear all;
%% Gegeben:
%%
a = 0.04
b = 0.03
v = 0.01
B = 1.2
N = 3
l = 0.05;

vvec = [v 0 0]';
Bvec = [0 0 B]';
%% b) Induktionsspannung
%%
dAdT = v * b;

Uind = - N*B*dAdT
%% c) Diagramm
%%
tSteps = 0:0.01:10;
Ut = zeros(size(tSteps));
Ut(tSteps < a/v) = Uind;
Ut(tSteps >= a/v & tSteps < (a/v + (l-a)/v)) = 0;
Ut(tSteps >= (a/v + (l-a)/v) & tSteps < 2*a/v + (l-a)/v) = -Uind;
plot(tSteps,Ut);
%% 
%% 
%%