%% Aufgabe 13

clear all;
%% Gegeben:
%%
C = 8e-9
L = 0.08
R = 0.5
Ueff = 0.7e3
w = 1e4
%% a) effektive Spannungsabf�lle
%%
XL = w*L
XC = 1/(w*C)
Zc = R + 1i*(XL - XC)
Z = abs(Zc)

U0 = Ueff * sqrt(2)
Ieff = U0/Z

UL = XL * Ieff
UC = XC * Ieff
UR = R * Ieff

%% 
%% 
%% 
%%