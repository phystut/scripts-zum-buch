%% Aufgabe 9

clear all;
%% Gegeben:
%%
a = 0.02
b = 0.03
f = 50
B = 0.05
N = 100
t = 0.003
%% a) Spitzenspannung
%%
mu0 = 1.2566e-6;
A = a*b;
U0 = N*B*A*2*pi*f
%% b) Spannung nach $t = 0.003s$
%%
U = U0 * sin(2*pi*f*t)
%% c) Wie lange bis zum Maximalwert?
%%
tmax = 1 / (2*2*f)
%Check
Utest = U0 * sin(2*pi*f*tmax)
%% 
%% 
%%