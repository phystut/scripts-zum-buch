%% Aufgabe 1

clear all;
%% Gegeben:
%%
B = 0.05 %T
r = 0.01 %1cm
mu0 = 1.2566e-6 %N/A^2
%% a) Feld zeichnen und Strom berechnen
%%
x = linspace(-0.05,0.05,10);
[X,Y,Z] = meshgrid(x);

[theta,rho] = cart2pol(X,Y,Z);

%Strom I
I = B*2*pi*r / mu0
Ivec = [0 0 I];
lvec = [0 0 x(end) - x(1)];

Bfield = mu0 * I ./ (2*pi*rho);
BfieldX = arrayfun(@(x,y,z) Ivec(2)*z - y*Ivec(3),X,Y,Z);
BfieldY = arrayfun(@(x,y,z) Ivec(3)*x - z*Ivec(1),X,Y,Z);
BfieldZ = arrayfun(@(x,y,z) Ivec(1)*y - x*Ivec(2),X,Y,Z);

vecNorm = sqrt(BfieldX.^2 + BfieldY.^2 + BfieldZ.^2);
BfieldX = Bfield .* BfieldX ./ vecNorm;
BfieldY = Bfield .* BfieldY ./ vecNorm;
BfieldZ = Bfield .* BfieldZ ./ vecNorm;

quiver3(X,Y,Z,BfieldX,BfieldY,BfieldZ);
line([0 0], [0 0], [x(1) x(1)+lvec(3)],'LineWidth',5,'Color','r');
hold on;
%contourslice(X,Y,Z,Bfield,[],[],0)
%% b) Lorentzkraft
%%
q = -2*1.609e-19 %-2e
d = r
v = 5e-3
FL = q*v*B
%% c) Rumgeeiere
%%
me = 9.10938356e-31;
dT = 1e-11;%0.05*me / (abs(q)*B)
steps = 100;
pos = [d 0 0.025];
vi = [0 0 v];
a = [0 0 0];

i = 1;
while true
    %h = plot3(pos(:,1),pos(:,2),pos(:,3));
    Bi(1) = interp3(X,Y,Z,BfieldX,pos(i,1),pos(i,2),pos(i,3));
    Bi(2) = interp3(X,Y,Z,BfieldY,pos(i,1),pos(i,2),pos(i,3));
    Bi(3) = interp3(X,Y,Z,BfieldZ,pos(i,1),pos(i,2),pos(i,3));
    
    if any(isnan(Bi))
        outOfWindow = true;
        break;
    end
    
    FL = q*cross(vi,Bi);
    a = FL./me;
    vi = vi + a.*dT;
    %vi = v /norm(vi) * vi; %Make sure it does not increase
    pos(i+1,:) = pos(i,:) + vi.*dT;
    
    out = ['Step ' num2str(i) ': pos=' num2str(pos(i,:)) ', B=' num2str(Bi) ', FL=' num2str(FL) ', a=' num2str(a) ',v=' num2str(v), 'new Pos after dT=' num2str(dT) ': ' num2str(pos(i+1,:))];
    disp(out);
    i = i+1;
    %h = plot3(pos(:,1),pos(:,2),pos(:,3));
    %delete(h);
end
h = plot3(pos(:,1),pos(:,2),pos(:,3));
view([-6.7000 10.0000])

%figure;
%opti = odeset('OutputFcn',@odeplot);
%start = [pos;vi]
%f = @(t,y) [y(4:6); (q/me) .* cross(y(4:6) , getWireField(I,y(1),y(2)))];
%[ts,pos] = ode45(f,[1 100],start)%,opti);
%plot3(pos(:,1),pos(:,2),pos(:,3))



%%