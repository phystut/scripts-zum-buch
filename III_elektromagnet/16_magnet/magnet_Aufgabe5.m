%% Aufgabe 5

clear all;
%% Gegeben:
%%
l = 0.2
d = 0.02
N = 1000
I = 75e-3

mu0 = 1.2566e-6
%% a) Magnefeld
%%
B0 = mu0 * N * I / l
%% b) Fluss
%%
Phi = B0 * pi * (d/2)^2
%% c) Eisenkern
%%
B = 0.47
mur = B * l / (N*I*mu0)
M = (mur - 1) * B / (mu0*mur)
%%