%% Aufgabe 2

clear all;
%% Gegeben:
%%
me = 9.10938356e-31
U = 1000
B = 0.2
q = 1.609e-19
%% a) Detektorpunkt
%%
Ekin = q*U
v = sqrt(2*Ekin / me)
r = me*v / (q*B)
d = 2*r
%% b) Einfach ionisiertes He
%%
qHe = q;
mHe = 6.64e-27
EkinHe = qHe*U
vHe = sqrt(2*EkinHe / mHe)
rHe = mHe*vHe / (qHe*B)
dHe = 2*rHe
%% c) St�rkeres Feld
%%
B = 2
Ekin = q*U
v = sqrt(2*Ekin / me)
r = me*v / (q*B)
d = 2*r