%% Aufgabe 4

clear all;
%% Gegeben:
%%
mur = 2500
B = 0.8
mu0 = 1.2566e-6
%% Magnetische Suszeptibilität
%%
chi_m = mur - 1
%% Magnetisierung
%%
M = chi_m * B / (mur*mu0)
%%