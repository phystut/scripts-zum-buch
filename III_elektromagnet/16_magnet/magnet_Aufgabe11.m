%% Aufgabe 11

clear all;
%% Gegeben:
%%
U1 = 230
U2 = 50e3
N1 = 50
%% a) Windungen Sekundärspule
%%
N2 = N1 * U2 / U1
N2 / N1
%% Überlandleitung
%%
Rinnen = 1
l = 100e3
d = 15e3
P = 220e3
%% b) Leistungsverlust
%%
Rges = Rinnen / l * d
Ieff2 = P / U2
Pweg2 = Ieff2^2 * Rges

Ieff1 = P / U1
Pweg1 = Ieff1^2 * Rges

Pweg2 / Pweg1
%% 
%% 
%% 
%%