%% Aufgabe 3

clear all;
%% Gegeben:
%%
d = 0.5e-3
I = 0.4
B = 0.5 
UH = 4.2e-8
e = 1.609e-19

%% Elektronendichte
%%
n = I*B / (UH*d*e)
%%