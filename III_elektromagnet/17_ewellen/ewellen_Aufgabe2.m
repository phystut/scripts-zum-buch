%% Aufgabe 2

clear all;
%% Gegeben:
%%
mu_luft = 1+4e-7
e_luft = 1.00059
mu_h2o = 0.999991
e_h2o = 80
mu_pet = 1.037
e_pet = 2

c = 299792458
%% Lichtgeschwindigkeiten und Brechungsindices
%%
cmed = @(mu,e)  1/sqrt(mu*e) * c
c_luft = cmed(mu_luft,e_luft)
c_h2o = cmed(mu_h2o,e_h2o)
c_pet = cmed(mu_pet,e_pet)

n_luft = c / c_luft
n_h2o = c / c_h2o
n_pet = c/ c_pet

%% 
%% 
%% 
%%