%% Elektrostatik - Aufgabe 2 
%% Gegebene Gr��en: Ladungen & Punkt

clear all;
q(1).c = 25e-9; %Coulomb
q(1).pos = 0.01*[0 4]; %cm

q(2).c = 0.1e-6;
q(2).pos = 0.01*[0 0];

q(3).c = -50e-9;
q(3).pos = 0.01*[-4 3];

e_0 = 8.854e-12; %C^2/(N m^2)

P = 0.01*[0 3];

windowMin = min([q(:).pos P]);
windowMax = max([q(:).pos P]);
% Plot
pos = vertcat(q(:).pos)';

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
hold on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');
hold off;

xlim([windowMin windowMax]);
ylim([windowMin windowMax]);
%% Teil a)
%% Kr�fte
%%
r_12 = norm(q(1).pos - q(2).pos)
F_12 = q(1).c*q(2).c / (4*pi*e_0*r_12^2)

r_23 = norm(q(3).pos - q(2).pos)
F_23 = q(3).c*q(2).c / (4*pi*e_0*r_23^2)

winkel_12 = rad2deg(cart2pol(q(1).pos(1) - q(2).pos(1),q(1).pos(2) - q(2).pos(2)))
winkel_23 = rad2deg(cart2pol(q(3).pos(1) - q(2).pos(1),q(3).pos(2) - q(2).pos(2)))

%Winkel aus aufgabe
alpha_23 = 180 - winkel_23;
alpha_12 = 180 - winkel_12;

%Negativ auf der y-Achse!
F_23_buch = [F_23 * cosd(alpha_23) -F_23 * sind(alpha_23)]
F_12_buch = [F_12 * cosd(alpha_12) -F_12 * sind(alpha_12)]



%Da der koordinatenursprung in der ausgesetzten ladung lag, vorzeichen umdrehen!
F_12 = -1 * [F_12 * cosd(winkel_12) F_12 * sind(winkel_12)]
F_23 = -1 * [F_23 * cosd(winkel_23) F_23 * sind(winkel_23)]


F_res = F_12 + F_23

%% 
%% Teil b)
%% Berechne die Abst�nde
%%
[X,Y] = meshgrid(linspace(windowMin,windowMax,20)); 


for i = 1:numel(q)
    %Polarkoordinaten (allgemein)
    [q(i).winkel, q(i).rAll] = cart2pol(X-q(i).pos(1),Y - q(i).pos(2));
    
    %Aufgabenspezifisch
    [q(i).winkel_p, q(i).r_p] = cart2pol(P(1) - q(i).pos(1), P(2) - q(i).pos(2));
    q(i).dist = P - q(i).pos;
    %q(i).r = norm(q(i).dist);
    fprintf('rp_%d = %f\n',i,q(i).r_p); 
end


E_feld = @(q,r) q ./ (4*pi*e_0*r.^2);

%% Beitr�ge der E-Felder
%%
%E-Felder Betr�ge
for i = 1:numel(q)
    q(i).E = E_feld(q(i).c,q(i).r_p);
    fprintf('E_%d = %g\n',i, q(i).E);
    
    q(i).EAll = E_feld(q(i).c,q(i).r_p);
end


%% Feldvektoren
%%
E_feldvec_x = @(E,winkel) E .* cos(winkel);
E_feldvec_y = @(E,winkel) E .* sin(winkel);

ExAll = zeros(size(X));
EyAll = zeros(size(Y));


%Feldkomponenten
for i = 1:numel(q)
    q(i).E_vec(1) = E_feldvec_x(q(i).E,q(i).winkel_p);
    q(i).E_vec(2) = E_feldvec_y(q(i).E,q(i).winkel_p);
    disp(q(i).E_vec)
    
    q(i).Ex_All = E_feldvec_x(q(i).EAll,q(i).winkel);
    q(i).Ey_All = E_feldvec_y(q(i).EAll,q(i).winkel);
    
    
    ExAll = ExAll + q(i).Ex_All;
    EyAll = EyAll + q(i).Ey_All;
    
    %quiver(X,Y,q(i).Ex_All,q(i).Ey_All);
end

%% Superposition und Endergebnis
%%
EP_All = vertcat(q(:).E_vec);
EP_Sup = sum(EP_All,1)

EP = norm(EP_Sup)

EAll = sqrt(ExAll.^2 + EyAll.^2);

contourf(X,Y,EAll); hold on;
colormap copper; colorbar;
quiver(X,Y,ExAll,EyAll);

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
hold on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');

xlim([windowMin windowMax]);
ylim([windowMin windowMax]);