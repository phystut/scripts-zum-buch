%% Wie rechne ich.. die Elektronenablenkung in einer Braun'schen R�hre?
%% Gegeben:
%%
clear all;
x = 0.01; %1cm
v_x = 9e6; %m/s

E_y = 1000; %N/C;
q = 1.602e-19; %C
m_e = 9.11e-31; %kg
g = 9.81; %m/s
d = 0.2; %20cm
%% Zeit
%%
t = x ./ v_x
%% Spielt die Gravitationskraft eine Rolle?
%%
F_el = q * E_y;
F_G = m_e * g;

verhaeltnis = F_el / F_G

%% Ablenkung
%%
%F == F_el
a = q*E_y / m_e

y_1 = 0.5*a*t^2

v_y = a * t

alpha = atand(v_y / v_x)

%% Schirm
%%
y_2 = d*tand(alpha)

y = y_1 + y_2