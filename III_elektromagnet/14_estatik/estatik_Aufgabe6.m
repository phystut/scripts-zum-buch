%% Elektrostatik - Aufgabe 6 
%% Gegebene Gr��en: Ladungen & Punkt

clear all;
q(1).c = 10e-9; %Coulomb
q(1).pos = 0.001*[0 2]; %cm

q(2).c = -4e-9; %C
q(2).pos = 0.001*[2 0];

q(3).c = -2.5e-9;
q(3).pos = 0.001*[4 2];

e_0 = 8.854e-12; %C^2/(N m^2)

P = 0.001*[2 4];

windowMin = min([q(:).pos P]);
windowMax = max([q(:).pos P]);
% Plot
pos = vertcat(q(:).pos)';

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
hold on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');
hold off;
%% Potentiale
%%
[X,Y] = meshgrid(linspace(windowMin,windowMax,20)); 
phi = @(q,r) q ./ (4*pi*e_0*r);

phiAll = zeros(size(X));

for i = 1:numel(q)
    %Polarkoordinaten (allgemein)
    [q(i).winkel, q(i).rAll] = cart2pol(X-q(i).pos(1),Y - q(i).pos(2));
    
    q(i).phiAll = phi(q(i).c,q(i).rAll);
    phiAll = phiAll + q(i).phiAll;
    
    %Aufgabenspezifisch
    q(i).dist = P - q(i).pos;
    q(i).r = norm(q(i).dist);
    fprintf('rp_%d = %f\n',i,q(i).r); 
    q(i).phi = phi(q(i).c,q(i).r);
    fprintf('phi_%d = %f\n',i,q(i).phi); 
end

%% Superposition und Endergebnis
%%
phi_P = sum([q(:).phi])

contourf(X,Y,phiAll); hold on;
colormap copper; colorbar;

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
hold on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');