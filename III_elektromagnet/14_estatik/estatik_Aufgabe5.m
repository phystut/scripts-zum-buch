%% Elektrostatik - Aufgabe 5   
%% Gegeben:
%%
clear all;

r1 = 1e-3;  %1mm
r2 = 1; %1m

q1 = 5e-12; %5 pC
q2 = 0.02e-9; %0.02 nC

r3 = 0.1;

%Zusätzliche Kosntanten
e_0 = 8.854e-12; %C^2 / (N m^2)

%% Potentiale
%%
phi = @(q,r) q ./ (4*pi*e_0*r);

phi1 = phi(q1,r1)
phi2 = phi(q1,r2)

%% Spannung
%%
U = phi1 - phi2
%% Energie
%%
W1 = q1 * U
%% Arbeit an Probeladung q2
%%
W2 = q2 * phi(q1,r3)