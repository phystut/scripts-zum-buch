%% Aufgabe 1
%% Gegeben:
%%
clear all;
d = 1 %m, Ladungsabstand

q_1 = 1 %C
q_2 = -3.5 * q_1

q = [q_1 q_2];

%Ben�tigt
e_0 = 8.854e-12; %C^2/(N m^2)
%% a)
%%
F = q_1 * q_2 / (4*pi*e_0*d^2)

x = -1.5:0.2:2.5;
y = -2:0.2:2;
[X,Y] = meshgrid(x,y);

q_1_pos = [0 0];
q_2_pos = q_1_pos + [d 0];

[q_1_theta, q_1_rho] = cart2pol(X - q_1_pos(1),Y - q_1_pos(2));
[q_2_theta, q_2_rho] = cart2pol(X - q_2_pos(1),Y - q_2_pos(2));

q_1_pot = q_1 ./ (4*pi*e_0*q_1_rho);
q_2_pot = q_2 ./ (4*pi*e_0*q_2_rho);


E_1 = q_1_pot ./ q_1_rho;
E_2 = q_2_pot ./ q_2_rho;

E_1_x = E_1 .* cos(q_1_theta);
E_2_x = E_2 .* cos(q_2_theta);

E_1_y = E_1 .* sin(q_1_theta);
E_2_y = E_2 .* sin(q_2_theta);

Ex = E_1_x + E_2_x;
Ey = E_1_y + E_2_y;

E_tot = sqrt(Ex.^2 + Ey.^2);
tot_pot = q_1_pot + q_2_pot;

contourf(X,Y, tot_pot);
colormap hot; colorbar; hold on;
streamslice(X,Y,Ex,Ey);
plot([q_1_pos(1),q_2_pos(1)],[q_1_pos(2),q_2_pos(2)],'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b')

%%