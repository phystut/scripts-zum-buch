%% Elektrostatik - Aufgabe 3
%% Gegeben:
%%
clear all;

e = 1.602e-19; %C
m_e = 9.11e-31; %kg
d = 0.01; %1cm durchlauf durch plattenkondensator
U_B = 1e3; %1kV
s = 0.002; %0,2 cm Plattenastand

%% Eintrittsgeschwindigkeit
%%
v_0 = sqrt(2*e*U_B / m_e)

%% Zeit
%%
t = d / v_0
%% Ablenkung
%%
U = m_e * s^2 * v_0^2 / e / d^2