%% Elektrostatik - Aufgabe 8   
%% Gegeben:
%%
clear all;

U = 8
C1 = 5e-12
C2 = 3 * C1
C3 = 0.01e-9
C5 = 1e-12
C4 = 0.5*C5
C6 = 0.5*C1
%% Kapazitšten
%%
C_Reihe = @(Cs) 1 / sum(1./Cs);
C_Parallel = @(Cs) sum(Cs);

Cers12 = C_Reihe([C1,C2])
Cers123 = C_Parallel([Cers12,C3])
Cers456 = C_Reihe([C4 C5 C6])
Cges = C_Reihe([Cers123 Cers456])  
%% Ladungen
%%
Qges = Cges * U

U4 = Qges / C4
U5 = Qges / C5
U6 = Qges / C6

U3 = Qges / Cers123
Q3 = U3 * C3
Q1 = U3*Cers12
Q2 = Q1