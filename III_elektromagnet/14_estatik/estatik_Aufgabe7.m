%% Elektrostatik - Aufgabe 7   
%% Gegeben:
%%
clear all;

s = 0.05;
d = 0.001;
U = 12;
e_r = 2;

%Zusätzliche Kosntanten
e_0 = 8.854e-12; %C^2 / (N m^2)

%% Teil a)
%% Kapazität 
%%
C = @(e_r,A,d) e_0*e_r*A/d;

C_a = C(e_r,s^2,d)
%% Ladung
%%
Q = C_a * U
%% Teil b)
%% Kapazitäten
%%
C_1 = C(e_r, s^2, d/2)
C_2 = C(1,s^2,d/2)
%% Gesamtkapazitäte (Reihenschaltung)

C_ers = C_1*C_2 / (C_1 + C_2)
%% Ladung
%%
Q = C_ers * U