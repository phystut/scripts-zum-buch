%% Elektrostatik - Aufgabe 4   
%% Gegeben:
%%
clear all;

d = 5; %m
E = 1000; %V/m

%% Fl�che
%%
A = (d/2)^2 * pi

%% Fluss
%%
Phi_el = E*A
%%