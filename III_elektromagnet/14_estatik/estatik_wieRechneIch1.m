%% Wie rechne ich.. die �berlagerung elektrischer Felder aus?
%% Ladungen & Punkt
%%
clear all;
q(1).c = 1; %Coulomb
q(1).pos = [0 0]; %cm

q(2).c = 3;
q(2).pos = [4 2];

q(3).c = -2;
q(3).pos = [3 4];

P = [0 4];

% Plot
windowMin = min([q(:).pos P]) - 1;
windowMax = max([q(:).pos P]) + 1;
pos = vertcat(q(:).pos)';

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
hold on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');

xlim([windowMin windowMax]);
ylim([windowMin windowMax]);

%% Berechne die Abst�nde
%%
[X,Y] = meshgrid(linspace(windowMin,windowMax,100));

%Abst�nde
for i = 1:numel(q)
    q(i).dist = P - q(i).pos;
    q(i).r = norm(q(i).dist);
    fprintf('r_%d = %f\n',i,q(i).r);
    
    q(i).rAll = arrayfun(@(x,y) norm([x y] - q(i).pos),X,Y);
end

e_0 = 8.854e-12; %C^2/(N m^2)
E_feld = @(q,r) q ./ (4*pi*e_0*r.^2);

%% Beitr�ge der E-Felder
%%
%E-Felder Betr�ge
for i = 1:numel(q)
    q(i).E = E_feld(q(i).c,q(i).r * 0.01); %r in m!
    fprintf('E_%d = %g\n',i, q(i).E);
    
    q(i).EAll = E_feld(q(i).c,q(i).r * 0.01);
end


%% Berechnung der Winkel (-> Polarkoordinaten)
%%
%Winkel
for i = 1:numel(q)
    q(i).winkel = arrayfun(@(x,y) cart2pol(x-q(i).pos(1),y - q(i).pos(2)),X,Y);
    
    %p_cos = [P(1) - q(i).pos(1)] / q(i).r;
    p_sin = [P(2) - q(i).pos(2)] / q(i).r;
    q(i).p_alpha = asind(p_sin);
    q(i).p_polar = cart2pol(P(1)-q(i).pos(1),P(2) - q(i).pos(2));
    fprintf('alpha_%d = %f\n',i,q(i).p_alpha);
end


%% Feldvektoren
%%
E_feldvec_x = @(E,winkel) E .* cos(winkel);
E_feldvec_y = @(E,winkel) E .* sin(winkel);

ExAll = zeros(size(X));
EyAll = zeros(size(Y));


%Feldkomponenten
for i = 1:numel(q)
    q(i).E_vec(1) = E_feldvec_x(q(i).E,q(i).p_polar);
    q(i).E_vec(2) = E_feldvec_y(q(i).E,q(i).p_polar);
    disp(q(i).E_vec)
    
    q(i).Ex_All = E_feldvec_x(q(i).EAll,q(i).winkel);
    q(i).Ey_All = E_feldvec_y(q(i).EAll,q(i).winkel);
    
    
    ExAll = ExAll + q(i).Ex_All;
    EyAll = EyAll + q(i).Ey_All;
    
    %quiver(X,Y,q(i).Ex_All,q(i).Ey_All);
end

%% Superposition und Endergebnis
%%
EP_All = vertcat(q(:).E_vec);
EP_Sup = sum(EP_All,1);
disp(EP_Sup);

EP = norm(EP_Sup);
disp(EP);

EAll = sqrt(ExAll.^2 + EyAll.^2);

contourf(X,Y,EAll);
colormap pink; colorbar;
%quiver(X,Y,ExAll,EyAll);
hold off;
streamslice(X,Y,ExAll,EyAll); hold on;

plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;

plot(P(1),P(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','k');

xlim([windowMin windowMax]);
ylim([windowMin windowMax]);