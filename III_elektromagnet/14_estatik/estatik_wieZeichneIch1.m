%% Elektrostatik - Aufgabe 2 
%% Gegebene Gr��en: Ladungen & Punkt

clear all;
q(1).c = 1; %Coulomb
q(1).pos = [-1 1];

q(2).c = -2;
q(2).pos = [1 0];

q(3).c = 1;
q(3).pos = [-1 -1];

e_0 = 8.854e-12; %C^2/(N m^2)

windowMin = -3;%min([q(:).pos]);
windowMax = 3;%max([q(:).pos]);
% Plot
pos = vertcat(q(:).pos)';

%plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
%box off;
%grid on;
%hold on;

[X,Y] = meshgrid(linspace(windowMin,windowMax,100)); 


for i = 1:numel(q)
    %Polarkoordinaten (allgemein)
    [q(i).winkel, q(i).rAll] = cart2pol(X-q(i).pos(1),Y - q(i).pos(2));    
end


E_feld = @(q,r) q ./ (4*pi*e_0*r.^2);

%% Beitr�ge der E-Felder
%%
%E-Felder Betr�ge
for i = 1:numel(q)
    q(i).EAll = E_feld(q(i).c,q(i).rAll);
end


%% Feldvektoren
%%
E_feldvec_x = @(E,winkel) E .* cos(winkel);
E_feldvec_y = @(E,winkel) E .* sin(winkel);

ExAll = zeros(size(X));
EyAll = zeros(size(Y));


%Feldkomponenten
for i = 1:numel(q)
    q(i).Ex_All = E_feldvec_x(q(i).EAll,q(i).winkel);
    q(i).Ey_All = E_feldvec_y(q(i).EAll,q(i).winkel);
    
    
    ExAll = ExAll + q(i).Ex_All;
    EyAll = EyAll + q(i).Ey_All;
    
    %subplot(1,numel(q),i);
    figure,streamslice(X,Y,q(i).Ex_All,q(i).Ey_All); hold on;
    plot(q(i).pos(1),q(i).pos(2),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
    
    box off;
    grid on;
    xlim([windowMin windowMax]);
    ylim([windowMin windowMax]);
    
    %quiver(X,Y,q(i).Ex_All,q(i).Ey_All);
end

%% Superposition und Endergebnis
%%
EAll = sqrt(ExAll.^2 + EyAll.^2);

%contourf(X,Y,EAll); hold on;
%colormap copper; colorbar;

figure
streamslice(X,Y,ExAll,EyAll); hold on;


plot(pos(1,:),pos(2,:),'LineStyle','None','Marker','o','MarkerSize',10,'MarkerFaceColor','b');
box off;
grid on;
xlim([windowMin windowMax]);
ylim([windowMin windowMax]);