%% Aufgabe 6
%% Gegeben:
%%
clear all;

U0a = 5 %V
U0b = 8 %V
U0c = 7 %V
R1 = 1 %Ohm
R2 = 5 %Ohm
R3 = 3.5 %Ohm
R4 = 2 %Ohm
R5 = 0.3 %Ohm
R6 = 4.8 %Ohm
%% Stromstärke Kirchoff
% Knoten: $I_1 = I_2 + I_3$
% 
% Masche $abef$: $-U_{0a} - R_1 I_1 - R_2 I_3 + U_{0b} - R_6 I_1 = 0 \Leftrightarrow 
% I_1 = \frac{U_{0b} - U_{0a} - R_2 I_3}{R_1 + R_6}$
% 
% Masche $bcde$: $-R_3 \underbrace{I_2}_{I_1 - I_3} - U_{0c} - R_{ers45} 
% \underbrace{I_2}_{I_1 - I_3} - U_{0b} + R_2 I_3 = 0 \Leftrightarrow I_1 = \frac{R_3 
% I_3 - U_{0c} + R_{ers45} I_3 - U_{0b} + R_2 I_3}{R_3 + R_{ers45}}$
% 
% Gleichsetzen:
% 
% $$\frac{U_{0b} - U_{0a} - R_2 I_3}{R_1 + R_6} = \frac{I_3 (R_3 +  R_{ers45} 
% + R_2)  - U_{0c} - U_{0b}}{R_3 + R_{ers45}}$$
% 
% $$\frac{U_{0b} - U_{0a}}{R_1 + R_6} - \frac{R_2 I_3}{R_1 + R_6} = \frac{I_3 
% (R_3 +  R_{ers45} + R_2)}{R_3 + R_{ers45}} -\frac{  U_{0c} + U_{0b}}{R_3 + R_{ers45}}$$
% 
% $$I_3 \left[\frac{(R_3 +  R_{ers45} + R_2)(R_1 + R_6) + R_2(R_3 + R_{ers45})}{(R_1 
% + R_6)(R_3 + R_{ers45})}\right] = \frac{(U_{0b} - U_{0a})(R_3 + R_{ers45}) + 
% (U_{0c} + U_{0b})(R_1 + R_6)}{(R_3 + R_{ers45})(R_1 + R_6)}$$
% 
% $$I_3 = \frac{(U_{0b} - U_{0a})(R_3 + R_{ers45}) + (U_{0c} + U_{0b})(R_1 
% + R_6)}{(R_3 +  R_{ers45} + R_2)(R_1 + R_6) + R_2(R_3 + R_{ers45})}$$

Rers45 = 1/sum(1./[R4 R5])

I3 = ((U0b - U0a)*(R3 + Rers45) + (U0c + U0b)*(R1 + R6))/((R3 + Rers45 + R2)*(R1 + R6) + R2*(R3+Rers45))

%Buch:
%I3 = ((-U0a)*(R3 + Rers45) + (U0c + U0b)*(R1 + R6))/((R3 + Rers45 + R2)*(R1 + R6) + R2*(R3+Rers45))

I1 = (I3*(R3 + Rers45 + R2) - U0c - U0b) / (R3 + Rers45)
I1_check = (U0b - U0a - R2*I3)/(R1 + R6)
I2 = I1 - I3

%% 
%