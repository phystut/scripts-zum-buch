%% Aufgabe 5
%% Gegeben:
%%
clear all;

U1 = 12 %V
U2 = 10 %V
Rinnen = 0.03 %Ohm
R = 0.04 %Ohm
%% Stromstärke Kirchoff
%%
%U1 - Rinnen*I - U2 - Rinnen*I - R * = 0
I = (U1 - U2) / (2*Rinnen + R)
%% Vertausche Polung
%%
%U1 - Rinnen*I + U2 - Rinnen*I - R * = 0
Itausch = (U2 + U1) / (2*Rinnen + R)