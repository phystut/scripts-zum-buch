%% Aufgabe 7: Auf- und Entladung eines Kondensators

clear all;
%% Gegeben:
%%
R = 100 %Ohm
C = 2e-9 %2nF
U = 50 %V
%% a) Aufladestrom bei $t=0$
%%
I0 = U / R
%% b) Maximale Ladung
%%
Qmax = C * U
%% c) Stromstärke bei $Q' = 0.75Q_{\max}$
% $$Q_C(t) = Q_{\max} \cdot \left(1 - e^{-\frac{t}{RC}}\right) \Rightarrow e^{-\frac{t}{RC}} 
% = 1 - \frac{Q_C}{Q_{\max}}$$

tau = R*C;
tPlot = linspace(0,5*tau,100);
Qc = Qmax * (1 - exp(-tPlot./tau));
plot(tPlot,Qc), xlabel('t'), ylabel('Q_C')

t = -tau * log(1 - 0.75)

I = I0*exp(-t/tau)
%% d) Zeitkonstante $\tau$
%%
tau
%% e) Entladungszeit bis $0.1Q_{\max}$
% $$Q_C(t) = Q_{\max}  e^{-\frac{t}{RC}}$$

t = -tau * log(0.1)
%% f)
%%
disp('Keine Ahnung, da war ich noch nicht...')
disp(repmat(['a'],100))