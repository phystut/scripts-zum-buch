%% Aufgabe 4

clear all;
%% Gegeben:
% Schaltung 1:

U1 = 10 %V
Rl_1 = 2 %Ohm
Rl_2 = 4 %Ohm
Rl_3 = 4.5 %Ohm
Rl_4 = 1.5 %Ohm
%% 
% Schaltung 2:

U2 = 12 %V
Rr_1 = 5.5 %Ohm
Rr_2 = 10 %Ohm
Rr_3 = 4 %Ohm
R_innen = 0.25 % Ohm
%% a) Ersatzwiderstand
% Schaltung 1:

Rl_ers34 = Rl_3 + Rl_4
Rl_ers = 1/sum(1 ./[Rl_1  Rl_2  Rl_ers34])
%% 
% Schaltung 2:

Rr_ers23 = 1/sum(1 ./ [Rr_2 Rr_3])
Rr_ers = Rr_1 + Rr_ers23
%% b) Gesamtstromstärke
% Schaltung 1:

Il_ges = U1/Rl_ers
%% 
% Schaltung 2:

Ir_ges = U2/Rr_ers
%% c) Einzelströme und Spannungsabfälle
% Schaltung 1:

Ul_1 = U1
Il_1 = Ul_1 / Rl_1
Ul_2 = U1
Il_2 = Ul_2 / Rl_2
Ul_ers34 = U1;
Il_3 = Ul_ers34 / Rl_ers34
Il_4 = Il_3
Ul_3 = Il_3 * Rl_3
Ul_4 = Il_4 * Rl_4
%% 
% Schaltung 2:

Ir_1 = Ir_ges

Ur_23 = Rr_ers23 * Ir_ges
Ur_2 = Ur_23
Ur_3 = Ur_23

Ur_1 = U2 - Ur_23
Ir_1 = Ur_1 / Rr_1
Ir_2 = Ur_2 / Rr_2
Ir_3 = Ur_3 / Rr_3
%% d) Leistung
% Schaltung 1:

Pl_1 = Ul_1 * Il_1
Pl_2 = Ul_2 * Il_2
Pl_3 = Ul_3 * Il_3
Pl_4 = Ul_4 * Il_4
%% 
% Schaltung 2:

Pr_1 = Ur_1 * Ir_1
Pr_2 = Ur_2 * Ir_2
Pr_3 = Ur_3 * Ir_3
%% e) Schalter
%%
Ur_1_schalter = 0
%% f) Amperemeter
%%
Rr_3ers_innen = Rr_3 + R_innen
Rr_ers23_innen = 1/sum(1 ./ [Rr_2 Rr_3ers_innen])
Ir_ges_innen = U2 / (Rr_ers23_innen + Rr_1);
Ur_23_innen = Rr_ers23_innen * Ir_ges_innen
Ir_2_innen = Ur_23_innen / Rr_2
Ir_3_innen = Ur_23_innen / Rr_3
Ur_innen = Ir_3_innen * R_innen