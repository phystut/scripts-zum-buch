%% Aufgabe 2
%% Gegeben:
%%
clear all;

rho = 1.7e-2*(1e-3)^2 %% 1.7e-2 Ohm mm^2 / m
d = 1.8e-3 % 1,8mm
l = 5 %m
T = 50 + 273 %K  , 50 �C
alpha = 3.9e-3 %1/K
I = 2 %A
T_Z = 293 %K, 20�C Zimmertemperatur
%% Kabelwiderstand
%%
A = (d/2)^2 * pi
Rinnen = rho * l / A
%% Temperaturerh�hung
%%
deltaT = T - T_Z
rho_T = rho * (1 + alpha*deltaT)
Rinnen_T = rho_T * l / A
%% Leistung
%%
U = Rinnen * I^2