%% Aufgabe 3
%% Gegeben:
%%
clear all;

R = 440 %Ohm
Ri = 0.2 %Ohm
I = 0.52 %A
%% Gesamtwiderstand
%%
Rges = R + Ri
%% Leistung
%%
Pges = Rges*I^2
Pi = Ri*I^2
PR = R*I^2