%% Aufgabe 1
%% Gegeben:
%%
clear all;

t = 3600 %1h
Q = 10^4 %C
n = 1.1e29 % 1/m^3
r = 1.13e-3 %1.13 mm 
e = 1.602e-19; %C
%% Dirftgeschwindigkeit
%%
I = Q/t
A = r^2*pi

vD = I/(n*e*A)
%% Stromdichte
%%
j = I/A