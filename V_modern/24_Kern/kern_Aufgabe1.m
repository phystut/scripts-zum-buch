%% Wie rechne ich... den Zerfall von Kernen?

clear all;
u = 1.6605e-27
c = 299792458
e = 1.609e-19
%% Massen
%%
mD = 2.0141018 * u
mT = 3.0160495 * u
mp = 1.0072765 * u
mn = 1.0086649 * u
%% Neutron Bindungsenergie
%%
dm = mD + mn - mT
udm = dm / u
Edm = dm * c^2
Edm_eV = Edm / e
%% Tritium Bindungsenergie
%%
dm = 2*mn + mp - mT
udm = dm / u
Edm = dm * c^2
Edm_eV = Edm / e