%% Aufgabe 2

clear all;
u = 1.6605e-27
c = 299792458
e = 1.609e-19
r0 = 1.3e-15
m131I = 131 * u
%% Gegeben
%%
t1 = 30 %Tage
tHalb_I = 8.0207 %Tage
%% a)
%%
N0 = 4.6e22
Nt = N0 * exp(-log(2) * t1 / tHalb_I)
mt = Nt *  m131I 
%% b)
%%
Nrel = 0.9334
t2 = 3 %Jahre
tHalb_Cs = - log(2) * t2 / log(Nrel)
%% c)
%%
 m0 = 1.874e-12
 tFisch = 2
 relGrenz = 0.1
 
 m2a = m0 * exp(-log(2) / tHalb_Cs * tFisch)
 
 tFischGut = -log(relGrenz) * tHalb_Cs / log(2)