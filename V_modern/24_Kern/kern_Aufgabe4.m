%% Aufgabe 2

clear all;
u = 1.6605e-27
c = 299792458
e = 1.609e-19
r0 = 1.3e-15
NA = 6.02214086e23
%% a)

Nrel = 0.533
tHalbC = 5730 %Jahre 

t = - tHalbC / log(2) * log(Nrel)
%% b)
%%
A = 1.81e8 %pro Jahr
mC12 = 0.030
mC12mol = 0.012
c14Rel = 1e-12
NC14 = A * tHalbC / log(2)

NC12 = mC12 / mC12mol * NA

N0C14 = NC12 * c14Rel

t = -tHalbC /log(2) * log(NC14 / N0C14)
%% c)
%%
t = - tHalbC / log(2) * log(1e-3)
%%