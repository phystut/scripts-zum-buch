%% Aufgabe 2

clear all;
u = 1.6605e-27
c = 299792458
e = 1.609e-19
r0 = 1.3e-15
%% Massenzahlen
%%
A = [4 12 108 238]
%% Kernradien
%%
r = r0 .* nthroot(A,3)