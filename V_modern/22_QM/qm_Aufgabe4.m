%% Aufgabe 4

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458
%% Gegeben:
%%
b = 0.08e-9
E = @(n) h^2 * n.^2 / (8*me*b^2)
%% a) Energieniveaus und Wellenl�ngen
%%
n = (1:5)
En = E(n)
En_ev = En / e
lambda = h*c ./ En
%% b) �bergangsfrequenzen
%%
E53 = En(5) - En(3);
E53_eV = E53 / e
f53 = E53 / h
E31 = En(3) - En(1);
E31_eV = E31 / e
f31 = E31 /h
E41 = En(4) - En(1);
E41_eV = E41 / e
f41 = E41 / h