%% Aufgabe 5

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
mp = 1.6727e-27
c = 299792458
%% Gegeben:
%%
v = 4e7
dv = 0.25 * v
%% Ortsunsicherheit
%%
dp = mp * dv
dx = h / (4*pi*dp)