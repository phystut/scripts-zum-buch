%% Aufgabe 5

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458
%% Gegeben:
%%
Ekin = 7 * e
m = 3e-11
%%  De-Broglie-Wellenlänge
%%
p = sqrt(2*Ekin*m)
l = h / p