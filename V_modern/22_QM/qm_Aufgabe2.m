%% Aufgabe 2

clear all;
e = 1.609e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458
%% Gegeben
%%
f = 10e18
theta = 180
%% a) Skizze
%% b) Wellenlängenzuwachs
%%
lambda_c = h / (me*c)
dLambda = lambda_c * (1 - cosd(theta))
lambdaNew = dLambda + c/f
%% c) Energie
%%
E = h * c / lambdaNew
E_eV = E / e
%% d) Elektron Geschwindigkeit
%%
%v = h/me*(f / c - 1/lambdaNew) 
p = h*f / c
pNew = E / c
v = (p - pNew) / me
%% 
%%