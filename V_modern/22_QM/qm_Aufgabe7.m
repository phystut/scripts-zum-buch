%% Aufgabe 7

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
mp = 1.6727e-27
c = 299792458
sb = 5.67e-8

%% Gegeben:
%%
T = 4000 %K
r = 1.75e10
%% Maximum
%%
lambdaMax = 2897.8e-6 / T
%% Strahlungsleistung
%%
A = 4*pi*r^2
P = sb * A * T^4