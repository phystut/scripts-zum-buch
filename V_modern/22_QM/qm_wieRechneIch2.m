%% Wie rechne ich... die Energieniveaus in einem Potentialtopf aus?

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458;
%% Gegeben:
%%
b = 0.2e-9
E = @(n) h^2 * n.^2 / (8*me*b^2)
%% Energieniveaus
%%
n = (1:5)'
En = E(n)
En_ev = En / e
%% Übergang
%%
deltaE = En(4) - En(1)
deltaE_ev = deltaE / e
%% Wellenlänge
%%
lambda = h*c / deltaE
%% 
%% 
%% 
%% 
%%