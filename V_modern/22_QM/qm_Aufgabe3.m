%% Aufgabe 3

clear all;
e = 1.609e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458
%% Gegeben
%%
lambda = 100e-9
EA = 4.3 * e
%% a) Max Energiezuwachs
%%
E = h * c / lambda
E_eV = E / e
Emax = E - EA
Emax_eV = Emax / e
%% c) Grenzfrequenz
%%
fg = EA / h
%% d) Nickel statt Silber
%%
U = 7.35
EU = e*U
EA_Ni = E - e*U
EA_Ni_eV = EA_Ni / e
%% e) Plot
%%
nuspace = linspace(0,3 * EA_Ni_eV/h,100);
EAg_eV = -EA / e + h*nuspace;
ENi_eV = -EA_Ni_eV + h*nuspace;
plot(nuspace,EAg_eV); hold on;
plot(nuspace,ENi_eV)
box off
grid on
line([nuspace(1) nuspace(end)],[0 0],'Color','k')
ylabel('E [eV]')
%xlabel('f [Hz]')
%xl = xticks;
%xl = str2double(xl) * e
%xl = num2str(xl)
%xticklabels(xl)
%% 
%%