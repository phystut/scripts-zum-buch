%% Wie rechne ich... die Austrittsarbeit beim Photoeffekt aus?

clear all;
e = 1.602e-19;
h = 6.626e-34;
%% Gegeben:
%%
nu = 1.4e15
Ekin = 0.8 * e; %0.8 eV
%% Photonenenergie

Eph = h*nu
Eph_eV = Eph / e
%% Austrittsenergie
%%
EA = Eph - Ekin
EA_eV = EA / e
%% Grenzfrequenz
%%
nuG = EA / h
%% 
%% 
%% 
%% 
%%