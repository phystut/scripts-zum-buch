%% Aufgabe 1)

clear all;
e = 1.602e-19;
h = 6.626e-34;
me = 9.11e-31;
c = 299792458;
Ry = 13.6 * e;
Rinf = 10973731.6;
a0 = 5.2918e-11;
muB = e * h /(4*pi*me); %Bohrsches Magneton
alpha = 1/137
%% Gegeben:
%%
Z = 1
%% Lyman
%%
n1 = 2
n2 = 1
E = -Ry * Z^2 * (1/n1^2 - 1/n2^2)
E_eV = E / e
f = E / h
%% Paschen
%%
n1 = 4
n2 = 3
E = -Ry * Z^2 * (1/n1^2 - 1/n2^2)
E_eV = E / e
f = E / h