%% Aufgabe 2

clear all;
u = 1.6605e-27
c = 299792458
e = 1.602e-19
h = 6.626e-34;
hQuer = h / (2*pi);
me = 9.11e-31;
k = 1.38064852e-23
%% Fermienergie

ne = 1.7e21 / (1e-2)^3
Ef = hQuer^2 / (2 * me) *(3 * pi^2 * ne)^(2/3)
Ef_eV = Ef / e
%% Wahrscheinlichkeit
%%
T = 293
f = @(E) (exp((E - Ef) ./ (k*T)) + 1).^(-1);

E2eV = Ef_eV + 0.01
p = f(E2eV*e)

EeV = linspace(0,2*Ef_eV,100);
fAll = f(EeV * e);
plot(EeV,fAll); hold on;
plot([Ef_eV Ef_eV],[0 1])
plot([0 E2eV E2eV], [p p 0],'k--')
xlabel('E [eV]');
ylabel('f(E)');
title('Fermi Dirac Statistik')
legend('f(E)', 'E_F', 'f(E_F + 0.01 eV)')