%% Aufgabe 1

clear all;
u = 1.6605e-27
c = 299792458
e = 1.602e-19

%% b) 
%%
rhoFe = 7.874e3

mFe = 55.845 * u;

mEZ = 2 *mFe,3 %mEZ = round(2 *mFe,3,'significant')

V = mEZ / rhoFe

a = nthroot(V,3)