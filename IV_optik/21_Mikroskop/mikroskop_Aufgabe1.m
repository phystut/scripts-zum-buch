%% Aufgabe 3

clear all;

%Konstante
s0 = 25e-2

%Gegeben
f = 5e-3
Vok = 10




%% a)
%%
fOk = s0/Vok
%% b)
%%
VGes = 400
Vobj = VGes / Vok
t = f * Vobj
%% 
%%
lBak = 6e-6
Vobj = 100
%% c)

tmp = 1/f - 1/(t + f)
g = tmp^(-1)
B = lBak*(t+f)/g
%%
lMegavir = 700e-9
lambda = 490e-9
n = 1.5
alpha = 60/2
%% d)

dmin = lambda/(n*sind(alpha))
dmin < lMegavir
%%
lambda = 550e-9
%% f)

dMin = 0.61*lambda / (n*sind(alpha))