%% Aufgabe 1

clear all;

%Konstanten
c = 3e8
alphaMaxDS = @(n,lambda,g) asind(n*lambda / g);
alphaMinDS = @(n,lambda,g) asind((2*n-1)*lambda/2 / g);
alphaMinES = @(n,lambda,b) asind(n*lambda / b);
alphaMaxES = @(n,lambda,b) asind((2*n+1)*lambda/2 / b);


%Gegeben
b = 80e-6
f = 400e12
lambda = c / f
a = 5
%% a)
%%
n = 1;
alpha = [];

while true
    alpha(end+1) = alphaMinES(n(end),lambda,b);
    if ~isreal(alpha(end))
        alpha = alpha(1:end-1);
        n = n(1:end-1);
        break;
    end
    %fprintf('n = %d: alpha = %f�\n',n,alpha);
    n(end+1) = n(end)+1;
end 
plot(n,alpha)
%% b) 
%%
n = 1
x = n*lambda*a / b
%% c)
%%
n = 5
x5 = n*lambda*a / b
x5_ex = tand(alpha(5))*a
x5 - x5_ex
%%