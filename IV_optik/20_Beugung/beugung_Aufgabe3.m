%% Aufgabe 3

clear all;

%Konstanten
c = 3e8
alphaMaxDS = @(n,lambda,g) asind(n*lambda / g);
alphaMinDS = @(n,lambda,g) asind((2*n-1)*lambda/2 / g);
alphaMinES = @(n,lambda,b) asind(n*lambda / b);
alphaMaxES = @(n,lambda,b) asind((2*n+1)*lambda/2 / b);


%Gegeben
g = 5e-6
lambdaMin = 400e-9
lambdaMax = 700e-9
a = 2
%% a)
%%
n = lambdaMin / (lambdaMax - lambdaMin)
%%