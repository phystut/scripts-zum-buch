%% Aufgabe 1

clear all;

%Konstanten
c = 3e8
alphaMaxDS = @(n,lambda,g) asind(n*lambda / g);
alphaMinDS = @(n,lambda,g) asind((2*n-1)*lambda/2 / g);
alphaMinES = @(n,lambda,b) asind(n*lambda / b);
alphaMaxES = @(n,lambda,b) asind((2*n+1)*lambda/2 / b);


%Gegeben
g = 20e-6
a = 2.8
dx = 6e-2
%% a)
%%
lambda = g*dx / (1*a)
%% b) 
%%
nMax = g / lambda
nMax = floor(nMax)

n = 1;
alpha = [];

while true
    alpha(end+1) = alphaMaxDS(n(end),lambda,g);
    if ~isreal(alpha(end))
        alpha = alpha(1:end-1);
        n = n(1:end-1);
        break;
    end
    %fprintf('n = %d: alpha = %f�\n',n,alpha);
    n(end+1) = n(end)+1;
end 
plot(n,alpha)

x = a*tand(alpha(end))
%% c)
%%
nEs = 1
nDs = 5
b = nEs * g/ nDs
%%