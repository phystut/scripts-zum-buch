%% Wie rechne ich... Positionen und Abst�nde der maxima und Minima beim Doppelspalt?

clear all;

%Konstanten
c = 3e8

%Gegeben
g = 100e-6
lambda = 600e-9
a = 1

alphaMax = @(n) asind(n*lambda / g);
alphaMin = @(n) asind((2*n-1)*lambda/2 / g);
%% Winkel Maximum & Minimum
%%
n = 2
alphaMax2 = alphaMax(2)
alphaMin2 = alphaMin(2)
%% Position Maximum
%%
x = n * lambda / g * a
%%