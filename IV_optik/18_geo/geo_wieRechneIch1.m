%% Wie rechne ich... ein optisches System mit mehreren Grenzfl�chen?

clear all;

%Konstanten
c = 3e8

%Gegeben
r = [0.1 -0.08 0.08 0.04]
n = [1 1.3 1.38 1.4 1.36]
g = 2

%% Brechkr�fte
%%
deltaN = diff(n)
D = deltaN ./ r 
Dtot = sum(D)
%% Brennweiten
%%
f_g = n(1) / Dtot
f_b = n(end) / Dtot
%% Bildweite
%%
b = n(end) / (Dtot - n(1)/g)