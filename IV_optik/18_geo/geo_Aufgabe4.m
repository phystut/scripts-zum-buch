%% Aufgabe 3

clear all;

%Konstanten
c = 3e8

%Gegeben
f1 = 50e-3
f2 = 35e-3
%% a) Sensorentfernung
%%
g = inf

b1 = (1 / f1 - 1 / g)^(-1)
b2 = (1 / f2 - 1 / g)^(-1)

b1 == f1
b2 == f2
%% b)
%%
g = 2
b = (1 / f1 - 1/g)^(-1)
deltaF = b - f2
%% c)
%%
dB = 36.6e-2
dS = 43.3e-2

%%