%% Aufgabe 3

clear all;

%Konstanten
c = 3e8

%Gegeben

r1 = -0.03
r2 = -0.075
nL = 1
nG = 2
nW = 1.33

G = 0.02
g = 0.09
%% b) Brechkraft & Brennweiten
%%
D1 = (nG - nL) / r1
D2 = (nW - nG) / r2

Dges = D1 + D2

f1 = nL / Dges
f2 = nW / Dges
%% c)
%%
b = nW * (Dges - nL / g)^(-1)
B = G * nL*b / (nW * g)
%%