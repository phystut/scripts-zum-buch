%% Wie rechne ich... ein optisches System mit mehreren Grenzfl�chen?

clear all;

%Konstanten
c = 3e8

%Gegeben

n0 = 1
n1 = 1.5
n2 = 1.2
n3 = 1.1
n4 = 1.8

alpha0 = 30
%% Winkel
%%
alpha1 = asind(n0*sind(alpha0)/n1)
beta = 90 - alpha1
Thetak = asind(n2/n1)
Thetak < beta
gamma1 = alpha1
gamma2 = asind(n1*sind(gamma1)/n3)
delta1 = gamma2
delta2 = asind(n3*sind(delta1)/n4)
epsi1 = delta2
epsi2 = asind(n4 * sind(epsi1) / n0)
%%