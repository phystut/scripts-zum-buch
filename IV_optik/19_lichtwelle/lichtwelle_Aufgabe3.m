%% Aufgabe 3

clear all;

%Konstanten
c = 3e8

%Gegeben
lambda = 200e-12
d = 250e-12

theta_n2 = 45.4
%% a)
%%
n = 0
thetaB = @(n) asind(0.5 * n*lambda / d);

while true
    theta_n = thetaB(n);
    if ~isreal(theta_n)
        break;
    end
    fprintf('n = %d: theta = %f�\n',n,theta_n);
    n = n+1;
end 
fprintf('Maximum Angle at n = %d\n',n-1)

%% b)

n = 2
d = 0.5*n*lambda / sind(theta_n2)
%%