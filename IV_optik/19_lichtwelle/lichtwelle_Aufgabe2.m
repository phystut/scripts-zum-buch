%% Aufgabe 2

clear all;

%Konstanten
c = 3e8

%Gegeben
alpha = 45
beta = 20
%% a)
%%
Irel = cosd(alpha)^2
%% b)
%%
Irel2 = cosd(beta)^2 * cosd(alpha - beta)^2
%% 
%%