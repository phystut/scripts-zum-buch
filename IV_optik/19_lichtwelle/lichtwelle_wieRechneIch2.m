%% Wie rechne ich... hintereinander angeordnete Polarisationsfilter?

clear all;

%Konstanten
c = 3e8

%Gegeben
alpha_1 = 45
alpha_2 = 65
%% Reihenfolge 1-2
%%
alpha_12 = alpha_2 - alpha_1
Irel = cosd(alpha_1)^2 * cosd(alpha_12)^2
%% Reihenfolge 2-1
%%
alpha_21 = alpha_1 - alpha_2
IrelRev = cosd(alpha_2)^2 * cosd(alpha_21)^2
%% 
%%