%% Wie rechne ich... Brechung und Dispersion am Prisma?

clear all;

%Konstanten
c = 3e8

%Gegeben
lambda_b = 440e-9
alpha_1 = 45
n_b = 1.65
n_r = 1.61
n_L = 1

gamma = 60
%% Erste Brechung
%%
beta_1 = asind(sind(alpha_1) * n_L / n_b)
%% Zweiter Einfallswinkel
%%
epsi = 180 - gamma
beta_2 = 180 - epsi - beta_1
%% Zweite Brechung
%%
alpha_2 = asind(sind(beta_2) * n_b / n_L)
%% Ablenkung
%%
delta_1 = alpha_1 + alpha_2 - gamma
%% Zweiter Winkel mit Gesamtormel
%%
delta_2 = alpha_1 + asind(sind(gamma - asind(sind(alpha_1) * n_L/n_r)) * n_r/n_L) - gamma
deltaDelta = delta_1 - delta_2
%% 
%%